#!/usr/bin/env dub
/+ dub.sdl:
    name "mu.loves-dick.pics"
    description "Music Recs"
    authors "Neo Ar"
    homepage "https://getsimple.computer"
    license "public domain or MIT"
    stringImportPaths "strings"
    dependency "msgpack-d" version="~>1.0.4"
+/
enum title = "Music Recs";
enum site = "https://mu.loves-dick.pics";
enum description = "Music recommendations";
enum install_dir = "~/mu";
void main(string[] args)
{
    immutable wd = install_dir.expandTilde;
    immutable posts = wd.buildPath("posts");
    immutable build = wd.buildPath("build");
    immutable rec_template = wd.buildPath("rec.template");
    immutable rss_item_template = wd.buildPath("rss_item.template");
    immutable atom_entry_template = wd.buildPath("atom_entry.template");
    immutable styles = wd.buildPath("styles.css");
    immutable output = wd.buildPath("index.html");
    immutable rss = wd.buildPath("rss.xml");
    immutable atom = wd.buildPath("atom.xml");
    immutable feed_db = wd.buildPath("feed.db");
    Feed feed; ubyte[] db;
    if (!feed_db.exists)
    {
        feed = Feed(title, site, description);
        feed.updated = cast (DateTime)Clock.currTime.toUTC;
    }
    else
    {
        db = cast (ubyte[])feed_db.read;
        feed = db.unpack!Feed;
        feed.updated = cast (DateTime)Clock.currTime.toUTC;
    }
    scope (exit) { db = feed.pack; feed_db.write(db); }
    scope (success) rss.write(feed.genRSSFeed);
    scope (success) atom.write(feed.genAtomFeed);
    string[string] mv;
    arraySep = ",";
    getopt(args, "mv", &mv);
    foreach (k,v; mv)
    {
        auto oldKey = "%s-%s".format(k.dirName.baseName, k.stripExtension.baseName);
        auto newKey = "%s-%s".format(v.dirName.baseName, v.stripExtension.baseName);
        auto entry = feed.items[oldKey];
        entry.updated = feed.updated;
        feed.items[newKey] = entry;
        feed.items.remove(oldKey);
        feed.published = feed.updated;
        auto old_o = build.buildPath(oldKey.setExtension("html"));
        auto old_o_rss = old_o.setExtension("rss");
        auto old_o_atom = old_o.setExtension("atom");
        auto o = build.buildPath(newKey.setExtension("html"));
        auto o_rss = o.setExtension("rss");
        auto o_atom = o.setExtension("atom");
        k.rename(v);
        old_o.rename(o);
        old_o_rss.rename(o_rss);
        old_o_atom.rename(o_atom);
    }
    auto index = appender!string; index.reserve(1<<20);
    scope (success) output.write(index_template.format(cast (string)styles.read, index[]));
    auto years = posts.dirEntries(SpanMode.shallow).array;
    years.sort!((a, b) => a.name > b.name);
    auto rec_tmp = cast (string)rec_template.read;
    foreach (string year; years)
    {
        auto section = appender!string; section.reserve(1<<20);
        auto recs = year.dirEntries(SpanMode.shallow).array;
        recs.sort!((a, b) => a.name < b.name);
        foreach (string rec; recs)
        {
            enforce(rec.extension == ".md");
            auto key = "%s-%s".format(year.baseName, rec.stripExtension.baseName);
            auto o = build.buildPath(key.setExtension("html"));
            auto o_rss = o.setExtension("rss");
            auto o_atom = o.setExtension("atom");
            string html_str;
            if (!o_rss.exists || rec.timeLastModified > o_rss.timeLastModified)
                ["pandoc --wrap=none --template=%s -t plain -o %s %s"
                    .format(rss_item_template, o_rss, rec)].esh;
            if (!o_atom.exists || rec.timeLastModified > o_atom.timeLastModified)
                ["pandoc --wrap=none --template=%s -t plain -o %s %s"
                    .format(atom_entry_template, o_atom, rec)].esh;
            if (!o.exists || rec.timeLastModified > o.timeLastModified)
            {
                o.write(rec_tmp.format(key));
                ["pandoc --wrap=none --template=%1$s %1$s |".format(rec),
                    "pandoc --wrap=none", "--template=%s".format(o),
                    "-f markdown+emoji", "-o", o].esh;
                html_str = cast (string)o.read;
                if (key in feed.items)
                {
                    feed.items[key].description = html_str;
                    feed.items[key].updated = feed.updated;
                }
                else
                {
                    auto item = FeedItem(html_str);
                    item.published = feed.updated;
                    item.updated = item.published;
                    feed.items[key] = item;
                }
                feed.published = feed.updated;
            }
            else html_str = cast (string)o.read;
            section ~= html_str;
        }
        auto s = section_template.format(year.baseName, section[]);
        index ~= s;
    }
}
enum index_template = import("index.template");
enum section_template = import("section.template");
enum rss_template = import("rss.template");
enum atom_template = import("atom.template");
struct Feed
{
    string title;
    string link;
    string description;
    DateTime published;
    DateTime updated;
    FeedItem[string] items;
}
struct FeedItem
{
    string description;
    DateTime published;
    DateTime updated;
}
string toRFC3339(DateTime date)
{
	return "%sZ".format(date.toISOExtString);
}
string toRFC2822(DateTime date)
{
    return "%s, %.2s %s %s %.2s:%.2s:%.2s +0000"
        .format(date.dayOfWeek.to!string.capitalize,
                date.day,
                date.month.to!string.capitalize,
                date.year,
                date.hour,
                date.minute,
                date.second);
}
string genRSSFeed(Feed feed)
{
    immutable build = install_dir.expandTilde.buildPath("build");
    auto entries = appender!string; entries.reserve(1<<20);
    auto items = feed.items.byPair.array;
    items.sort!((a, b) => a.value.updated > b.value.updated);
    foreach (kv; items)
    {
        auto o_rss = build.buildPath(kv.key.setExtension("rss"));
        auto rss_str = cast (string)o_rss.read;
        rss_str = rss_str.format("%s/#%s".format(site, kv.key),
                kv.value.published.toRFC2822,
                kv.value.description);
        entries ~= rss_str;
    }
    return rss_template
        .format(title, site, description, feed.updated.toRFC2822,
                feed.published.toRFC2822, entries[]);
}
string genAtomFeed(Feed feed)
{
    immutable build = install_dir.expandTilde.buildPath("build");
    auto entries = appender!string; entries.reserve(1<<20);
    auto items = feed.items.byPair.array;
    items.sort!((a, b) => a.value.updated > b.value.updated);
    foreach (kv; items)
    {
        auto o_atom = build.buildPath(kv.key.setExtension("atom"));
        auto atom_str = cast (string)o_atom.read;
        immutable entry_link = "%s/#%s".format(site, kv.key);
        atom_str = atom_str.format(entry_link, entry_link,
                kv.value.updated.toRFC3339,
                kv.value.published.toRFC3339,
                kv.value.description);
        entries ~= atom_str;
    }
    return atom_template
        .format(title, site, description, feed.updated.toRFC3339, entries[]);
}
string esh(string[] cmd)
{
    auto c = cmd.join(" ");
    c.writeln;
    auto o = c.executeShell;
    enforce(!o.status, o.output);
    return o.output;
}
import std;
import std.file : write;
import msgpack;
