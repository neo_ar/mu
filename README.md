# Music Recs Static Site Generator
* Requires the usual dlang stuff (dmd, libphobos, dub)
* Requires pandoc
* Folder paths are hardcoded, see top of gen.d file
* Expects markdown files like `posts/$YEAR/cool-band-album.md`
* Run ./gen.d and it spits out index.html, rss.xml, and atom.xml

Example:
```md
---
title: Amyl and the Sniffers – Comfort to Me
---
### [$title$](https://amylandthesniffers.bandcamp.com/album/comfort-to-me)
##### :calendar: September 10 :label: Punk Rock, Garage Punk
:star: ESSENTIAL

> 'Comfort To Me' sounds like it could be played in a rowdy Australian pub the band are used to – or a colossal arena.
— [Clash](https://www.clashmusic.com/reviews/amyl-and-the-sniffers-comfort-to-me/)
```
